import click
from selenium import webdriver
import time

@click.command()
@click.option("--login", default="login", help="You login on github.com")
@click.option("--passwd", default="pass", help="You password on github.com")
@click.option("--search", default="git", help="Search rep in girhub by update")
@click.option("--range_pages", default=2, help="range git pages")
def main(login, passwd, search, range_pages):
    suc_star = 0
    driver = webdriver.Chrome()
    driver.get("https://github.com/login")

    username = driver.find_element_by_name("login")
    username.send_keys(login)

    passgit = driver.find_element_by_name("password")
    passgit.send_keys(passwd)
    passgit.submit()

    t0 = time.time()

    driver.get("https://github.com/" + login + "?tab=stars")
    for i in range(range_pages):
        time.sleep(0.4)
        for j in range(30):
            star = driver.find_elements_by_xpath("//button[@data-ga-click='Repository, click unstar button, action:users#show; text:Unstar']")
            star[j].click()
            suc_star += 1
            time.sleep(0.1)
        driver.find_element_by_class_name("next_page").click()

    t1 = time.time()
    total = t0 - t1
    driver.close()
    print("Git star successfully {}, time {}".format(suc_star, total))

if __name__ == "__main__":
    main()
